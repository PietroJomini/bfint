> Pietro Jomini :comet:

# BFint

##### Js module for BF interpreting



## [Brainfuck](https://esolangs.org/wiki/Brainfuck)

> Brainfuck operates on an array of memory cells, also referred to as the tape, each initially set to zero. There is a pointer, initially pointing to the first memory cell.

##### STD

| Commands | Description                                                  |
| :------: | :----------------------------------------------------------- |
|   `>`    | Move the pointer to the right                                |
|   `<`    | Move the pointer to the left                                 |
|   `+`    | Increment the memory cell under the pointer                  |
|   `-`    | Decrement the memory cell under the pointer                  |
|   `.`    | Output the character signified by the cell at the pointer    |
|   `[`    | Jump past the matching `]` if the cell under the pointer is 0 |
|   `]`    | Jump back to the matching `[` if the cell under the pointer is nonzero |

##### ADDONS

| Commands | Description                                                  |
| :------: | :----------------------------------------------------------- |
|   `*`    | Output the numeric value signified by the cell at the pointer |



## Usage

##### Import the `Brainfuck` class

```js
import Brainfuck from ' Your path to module ';
let bf = new Brainfuck({
	bindSTD: true
});
```



##### Hello World

```js
let hw = '++++++++[>++++[>++>+++>+++>+<<<<-]>+>+>->>+[<]<-]>>.>---.+++++++..+++.>>.<-.<.+++.------.--------.>>+.';
bf.parse(hw).then(result => {
    console.log(result);
});
// Output 'Hello WOrld!'
```



##### Bind new actions

We define each interpretation of `namespaces`, or brainfuck commands, as an `action`.

```js
// Define the behaviour of '+'
bf.bind('+', (tape, ptr, out, iter) => {
    if (!tape[ptr]) tape[ptr] = 0;
    tape[ptr] += 1;
    return [tape, ptr, out, iter];
});

// Define the behaviour of ']'
bf.bind(']', (tape, ptr, out, iter, inp) => {
    let k = iter;
    if (tape[ptr] != 0) {
        let delta = -1;
        while (delta != 0) {
            k -= 1;
            if (inp[k] == '[') delta += 1;
            if (inp[k] == ']') delta -= 1;
        }
    }
    return [tape, ptr, out, k];
});
```

where:

- `tape`→ brainfuck tape 
- `ptr`→ pointer to a cell of `tape`
- `out`→ current output of the `parse` function
- `iter`→ current position of the parser int the `inp`
- `inp`→ input of the `parse` function
- `return` → **must** return an array with the new [`tape`, `ptr`, `out`] 



##### Override actions

Since the `bind` method doesn't allow us to add multiple `actions` for the same `namespace`, we have to `override` an `action` to change it's behaviour.

```js
// Override the behaviour of '*'
bf.override('*', (tape, ptr, out, iter) => {
	if (!tape[ptr]) tape[ptr] = 0;
    out += tape[ptr] + 'BF!';
    return [tape, ptr, out, iter]
});

bf.parse('++*').then(result => {
    console.log(result);
});
// Output '2BF!'
```

A `override` callback have the same limitations as the `bind` ones.


 ##### STD functions

`Brainfuck` class provide the implementation of all the standard command, plus some addons. To include them, we have to use the option `bindSTD`.

```js
let bf = new Brainfuck({
	bindSTD: true,			// Bind the STD commands
    bindADDONS: true		// Bind the addons commands
});
```

