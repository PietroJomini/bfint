import { STD, ADDONS } from "./STD.mjs";

export default class Brainfuck {
    
    constructor (opt) {

        if (!opt) opt = {};

        this.tape = [];
        this.cmds = [];
        this.ptr = 0;

        if (opt.bindSTD) STD.forEach(cmd => this.bind(cmd.namespace, cmd.action));
        if (opt.bindADDONS) ADDONS.forEach(cmd => this.bind(cmd.namespace, cmd.action));

    }

    bind(namespace, action) {
        if (namespace && action && typeof namespace == 'string' && typeof action == 'function' && namespace.length == 1 && this.cmds.filter(el => el.namespace == namespace).length <= 0) {
            this.cmds.push({
                namespace, action
            })
            return true;
        }
        return false;
    }

    override(namespace, action) {
        this.cmds.map(cmd => {
            if (cmd.namespace == namespace) cmd.action = action;
        });
    }

    parse(bfstr, opt) {
        return new Promise((resolve, reject) => {
            if (!bfstr) reject('Void input');
            this.output = '';
            let bfarr = Array.from(bfstr);
            this.parseIterator = 0;
            while (this.parseIterator < bfarr.length) {
                let e = bfarr[this.parseIterator];
                this.cmds.forEach(cmd => {
                    if (e == cmd.namespace) [this.tape, this.ptr, this.output, this.parseIterator] = cmd.action(this.tape, this.ptr, this.output, this.parseIterator, bfstr);
                });
                this.parseIterator += 1;
            }
            this.parseIterator = null;
            resolve(this.output, this.tape);
        });
    }
}