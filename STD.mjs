export const STD = [
    {
        namespace: '+',
        action: (tape, ptr, out, iter) => {
            if (!tape[ptr]) tape[ptr] = 0;
            tape[ptr] += 1;
            return [tape, ptr, out, iter];
        }
    },
    {
        namespace: '-',
        action: (tape, ptr, out, iter) => {
            if (!tape[ptr]) tape[ptr] = 0;
            tape[ptr] -= 1;
            return [tape, ptr, out, iter];
        }
    },
    {
        namespace: '>',
        action: (tape, ptr, out, iter) => {
            return [tape, ptr + 1, out, iter];
        }
    },
    {
        namespace: '<',
        action: (tape, ptr, out, iter) => {
            return [tape, ptr - 1, out, iter];
        }
    },
    {
        namespace: '.',
        action: (tape, ptr, out, iter) => {
            if (!tape[ptr]) tape[ptr] = 0;
            out += String.fromCharCode(tape[ptr]);
            return [tape, ptr, out, iter];
        }
    },
    {
        namespace: '[',
        action: (tape, ptr, out, iter, inp) => {
            let k = iter;
            if (tape[ptr] == 0) {
                let delta = 1;
                while (delta != 0) {
                    k += 1;
                    if (inp[k] == '[') delta += 1;
                    if (inp[k] == ']') delta -= 1;
                }
            }
            return [tape, ptr, out, k];
        }
    },
    {
        namespace: ']',
        action: (tape, ptr, out, iter, inp) => {
            let k = iter;
            if (tape[ptr] != 0) {
                let delta = -1;
                while (delta != 0) {
                    k -= 1;
                    if (inp[k] == '[') delta += 1;
                    if (inp[k] == ']') delta -= 1;
                }
            }
            return [tape, ptr, out, k];
        }
    }
]

export const ADDONS = [
    {
        namespace: '*',
        action: (tape, ptr, out, iter) => {
            if (!tape[ptr]) tape[ptr] = 0;
            out += tape[ptr];
            return [tape, ptr, out, iter];
        }
    }
]